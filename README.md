# brawlhalla-api

A Node.js wrapper for the [Brawlhalla API](http://dev.brawlhalla.com).

## Table of contents

- [Installation and Basic Usage](#installation)
- [Static Data](#data)
- [Methods](#methods)

## Change Log

Updating to v4 after migrating the code to TypeScript.

Updating to v3 once again restructures data. See [Static Data](#data) for details.

Updating to v2 changes how the package is initialized. The API key should now be supplied along with the `require('brawlhalla-api')(YourApiKey)` statement.

## Installation

Install via npm as with any other package:

```bash
$ npm install --save brawlhalla-api
```

Within your script:

```typescript
import { BrawlhallaApi } from 'brawlhalla-api';

const api: BrawlhallaApi = new BrawlhallaApi('your-api-key', 'your-steam-api-key');
```
The steam api key is currently a work in progress, and is optional.
It only is required if you are using the steam profile link to BHID features.

## Data
You can find semi-static data in the _data_ folder:
```typescript
import { legends } from 'brawlhalla-api/data/legends';
import { weapons } from 'brawlhalla-api/data/weapons';
```

You can also access legends easily using LegendUtils
```typescript
import { LegendUtils } from 'brawlhalla-api/utils/LegendUtils';

const legendById = LegendUtils.findLegendById(1);
const legendByName = LegendUtils.findLegendByName('Bodvar');
const legendByApproximateName = LegendUtils.findLegendByApproximateName('ragnor');
```

# TODO: Update the remaining documentation

## Methods

### .getSteamId(SteamProfileUrl)

```js
bh.getSteamId(SteamProfileUrl).then(function(steamID){

}).catch(function(error){

});
```

*Does not use a Brawlhalla API call.*

### .getBhidBySteamId(steamID)

```js
bh.getBhidBySteamId(steamID).then(function(bhid){

}).catch(function(error){

});
```

*Uses one Brawlhalla API call.*

### .getBhidBySteamUrl(steamProfileUrl)

```js
bh.getBhidBySteamUrl(steamProfileUrl).then(function(bhid){

}).catch(function(error){

});
```

*Uses one Brawlhalla API call.*

### .getPlayerStats(bhid)

```js
bh.getPlayerStats(bhid).then(function(playerStats){

}).catch(function(error){

});
```

*Uses one Brawlhalla API call.*

### .getPlayerRanked(bhid)

```js
bh.getPlayerRanked(bhid).then(function(playerRanked){

}).catch(function(error){

});
```

*Uses one Brawlhalla API call.*

### .getLegendInfo(legend)

`legend` may be the legend id or the legend name.

```js
bh.getLegendInfo(legend).then(function(legendInfo){

}).catch(function(error){

});
```

*Uses one Brawlhalla API call.*

### .getLegendByName(legendName)

Alias for `.getLegendInfo()`.

### .getClanStats(clanId)

```js
bh.getClanStats(clanId).then(function(clanStats){

}).catch(function(error){

});
```

*Uses one Brawlhalla API call.*

### .getRankings(options)

```js
bh.getRankings(options).then(function(rankings){

}).catch(function(error){

});
```
The `options` object, along with each of its properties, is optional. Default values are as follows:

```js
options = {
    "bracket": "1v1",
    "region": "all",
    "page": 1,
    "name": null
};
```

*Uses one Brawlhalla API call.*

### .getBhidByName(name)

*Returns exact name matches only.*

```js
bh.getBhidByName(name).then(function(users){

}).catch(function(error){

});
```

*Uses one Brawlhalla API call.*
