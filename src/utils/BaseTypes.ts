export const isNumeric: (value: {}) => value is number = (value: {}): value is number =>
  typeof value === 'number';
