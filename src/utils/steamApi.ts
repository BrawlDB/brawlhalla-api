// tslint:disable:no-unused-variable
export class SteamApi {
  /**
   * The API url
   */
  private static readonly apiUrl: string = 'TODO';

  /**
   * The api key
   * Used in many requests on the Steam API
   */
  private _steamApiKey: string;

  public set steamApiKey(apiKey: string) {
    this._steamApiKey = apiKey;
  }

  public constructor(steamApiKey: string) {
    this._steamApiKey = steamApiKey;
  }

  public async getSteamId(profileUrl: string): Promise<string> {
    throw new Error('Unimplemented method');
  }
}
