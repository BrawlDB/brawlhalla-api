import * as levenshtein from 'fast-levenshtein';

import { legends } from '../data/legends';
import { sword } from '../data/weapons';
import { Legend } from '../models/legend';

export class LegendUtils {
  public static findLegendById(legendId: number): Legend {
    let legendFound: Legend | undefined = LegendUtils.legends.find(
      (legend: Legend) => legend.id === legendId
    );
    if (legendFound === undefined) {
      console.warn('Legend not found! Unknown ID given');
      legendFound = new Legend(legendId, 'Unknown', sword, sword);
    }

    return legendFound;
  }

  public static findLegendByName(legendName: string): Legend {
    let legendFound: Legend | undefined = LegendUtils.legends.find(
      (legend: Legend) => legend.name === legendName
    );
    if (legendFound === undefined) {
        console.warn('Legend not found! Unknown name given');
        legendFound = new Legend(-1, legendName, sword, sword);
    }

    return legendFound;
  }

  public static findLegendByApproximateName(legendToFindName: string): Legend {
    if (LegendUtils.legendSegmentMap.has(legendToFindName)) {
      const matchingLegend: Legend | undefined = LegendUtils.legendSegmentMap.get(legendToFindName);
      if (matchingLegend === undefined) {
        throw new Error('Something went really wrong!');
      }

      return matchingLegend;
    }

    let shortestDistance: number = -1;
    let legendWithShortestDistance: Legend | undefined;

    LegendUtils.legendSegmentMap.forEach((legend: Legend, legendSegment: string) => {
      const currentDistance: number = levenshtein.get(legendSegment, legendToFindName);
      if (shortestDistance === -1 || currentDistance < shortestDistance) {
        shortestDistance = currentDistance;
        legendWithShortestDistance = legend;
      }
    });

    if (legendWithShortestDistance === undefined) {
      throw new Error('Something went really wrong!');
    }

    const shortestAllowedDistance: number = 3;
    if (shortestDistance > shortestAllowedDistance) {
      console.warn(`Distance is over ${shortestAllowedDistance}! Weird results can be expected`);
    }

    return legendWithShortestDistance;
  }

  private static readonly legends: Legend[] = legends;
  private static readonly legendSegmentMap: Map<string, Legend> = legends.reduce(
    (legendNameSegments: Map<string, Legend>, legend: Legend) => {
      legendNameSegments.set(legend.name, legend);
      legend.name.split(' ').forEach((legendNameSegment: string) => {
        legendNameSegments.set(legendNameSegment, legend);
      });

      return legendNameSegments;
    },
    new Map()
  );
}
