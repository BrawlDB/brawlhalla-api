import Axios, { AxiosRequestConfig, AxiosResponse } from 'axios';

export interface IRequestArgument {
  [key: string]: string;
}

export class BrawlhallaRequest {
  private readonly apiKey: string;
  private readonly apiUrl: string = 'https://api.brawlhalla.com';

  public constructor(apiKey: string, apiUrl: string) {
    this.apiKey = apiKey;
    this.apiUrl = apiUrl;
  }

  public doRequest<T>(pathArguments: string[], params?: IRequestArgument): Promise<T> {
    const requestParameters: AxiosRequestConfig =
      params !== undefined ? { params } : { params: {} };
    // tslint:disable-next-line:no-unsafe-any
    requestParameters.params.api_key = this.apiKey;

    return (Axios.get<T>(`${this.apiUrl}/${pathArguments.join('/')}`, requestParameters) as Promise<AxiosResponse<T>>)
      .then((response: AxiosResponse<T>): Promise<T> => {
        return Promise.resolve(this.parseUtf8Recursive<T>(response.data));
      });
  }

  // tslint:disable-next-line:no-any
  private parseUtf8Recursive<T>(objectToConvert: any): T {
    Object.keys(objectToConvert).forEach((key: string): void => {
      if (typeof objectToConvert[key] === 'object') {
        objectToConvert[key] = this.parseUtf8Recursive(objectToConvert[key]);
      } else if (typeof objectToConvert[key] === 'string') {
        try {
          objectToConvert[key] = decodeURIComponent(escape(objectToConvert[key]));
        } catch {
          // Most likely truncated utf8, show raw name instead of failing
        }
      }
    });

    return objectToConvert;
  }
}
