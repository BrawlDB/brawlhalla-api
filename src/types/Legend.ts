import { Weapon } from './Weapon';
import { TLegendName } from './types';

// TODO: Add description, lore, stats, skins, etc.
export interface Legend {
  id: number;
  name: TLegendName;
  firstWeapon: Weapon;
  secondWeapon: Weapon;
}
