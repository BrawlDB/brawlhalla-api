import { TWeaponName } from './types';

export interface Weapon {
  id: number;
  name: TWeaponName;
}
