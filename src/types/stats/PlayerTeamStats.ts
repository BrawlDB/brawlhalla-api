import { TValidDivision, TValidRegions, TValidTier } from '../types';

export interface PlayerTeamStats {
  brawlhallaIdOne: number;
  brawlhallaIdTwo: number;

  teamName: string;
  region: TValidRegions;

  rating: number;
  peakRating: number;
  tier: TValidTier;
  division: TValidDivision;
  globalRank: number;

  games: number;
  wins: number;

  brawlhallaNameOne: string;
  brawlhallaNameTwo: string;

  defeats: number;
}
