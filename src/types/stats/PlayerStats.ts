import { SimpleClan } from '../clan/SimpleClan';
import { LegendStats } from './LegendStats';
import { WeaponStats } from './WeaponStats';

export interface PlayerStats {
  brawlhallaID: number;
  name: string;
  xp: number;
  level: number;
  xpPercentage: number;
  games: number;
  wins: number;

  // Damage
  damageDealtByBomb: number;
  damageDealtByMine: number;
  damageDealtBySpikeball: number;
  damageDealtBySidekick: number;

  snowballHits: number;

  // Kos
  koByBomb: number;
  koByMine: number;
  koBySpikeball: number;
  koBySidekick: number;
  koBySnowball: number;

  legendStats: LegendStats[];
  weaponStats: WeaponStats[];
  clan?: SimpleClan;
}
