import { Legend } from '../Legend';
import { TValidDivision, TValidTier } from '../types';

export interface LegendRankedStats {
  legend: Legend;
  rating: number;
  peakRating: number;

  tier: TValidTier;
  division: TValidDivision;

  games: number;
  wins: number;
  defeats: number;
}
