import { Weapon } from '../Weapon';

export interface WeaponStats {

  weapon: Weapon;
  time: number;
  games: number;
  wins: number;
  kos: number;

}
