import { LegendRankedStats } from './LegendRankedStats';
import { PlayerTeamStats } from './PlayerTeamStats';
import { TValidDivision, TValidRegions, TValidTier } from '../types';

export interface PlayerRankedStats {
  brawlhallaID: number;
  name: string;

  region: TValidRegions;
  globalRank: number;
  regionRank: number;

  rating: number;
  peakRating: number;
  tier: TValidTier;
  division: TValidDivision;

  legends: LegendRankedStats[];
  teams: PlayerTeamStats[];
}
