import { Legend } from '../Legend';
import { TValidDivision, TValidRegions, TValidTier } from '../types';

export interface PlayerRanking {
  brawlhallaID: number;
  name: string;
  rank: number;
  bestLegend: Legend;
  bestLegendGames: number;
  bestLegendWins: number;
  rating: number;
  peakRating: number;
  tier: TValidTier;
  division: TValidDivision;
  games: number;
  wins: number;
  region: TValidRegions;
}
