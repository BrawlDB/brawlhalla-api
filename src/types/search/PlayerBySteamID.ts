export interface PlayerBySteamID {
  brawlhallaID: number;
  name: string;
}
