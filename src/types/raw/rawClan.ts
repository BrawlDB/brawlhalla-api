import { TClanRank } from '../index';

export interface RawClan {
  clan_id: number;
  clan_name: string;
  clan_create_date: number;
  clan_xp: string;
  clan: RawClanMember[];
}

export interface RawClanMember {
  brawlhalla_id: number;
  name: string;
  rank: TClanRank;
  join_date: number;
  xp: number;
}
