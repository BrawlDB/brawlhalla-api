import { TValidRegions, TValidTier } from "../types";

export interface RawPlayerRankedStats {
  name: string;
  brawlhalla_id: number;
  rating: number;
  peak_rating: number;
  tier: TValidTier;
  wins: number;
  games: number;
  region: TValidRegions;
  global_rank: number;
  region_rank: number;
  legends: RawPlayerRankedLegendStats[];
  '2v2': Raw2v2Team[];
}

export interface RawPlayerRankedLegendStats {
  legend_id: number;
  legend_name_key: string;
  rating: number;
  peak_rating: number;
  tier: TValidTier;
  wins: number;
  games: number;
}

export interface Raw2v2Team {
  brawlhalla_id_one: number;
  brawlhalla_id_two: number;
  rating: number;
  peak_rating: number;
  tier: TValidTier;
  wins: number;
  games: number;
  teamname: string;
  region: number;
  global_rank: number;
}
