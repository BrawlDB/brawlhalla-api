// Response from api player/{id}/stats
export interface IRawPlayerStats {
  brawlhalla_id: number;
  name: string;
  xp: number;
  level: number;
  xp_percentage: number;
  games: number;
  wins: number;
  damagebomb: string;
  damagemine: string;
  damagespikeball: string;
  damagesidekick: string;
  hitsnowball: number;
  kobomb: number;
  komine: number;
  kospikeball: number;
  kosidekick: number;
  kosnowball: number;
  legends: IPlayerStatsLegend[];
  clan?: IPlayerStatsClan;
}

export interface IPlayerStatsLegend {
  legend_id: number;
  legend_name_key: string;
  damagedealt: string;
  damagetaken: string;
  kos: number;
  falls: number;
  suicides: number;
  teamkos: number;
  matchtime: number;
  games: number;
  wins: number;
  damageunarmed: string;
  damagethrownitem: string;
  damageweaponone: string;
  damageweapontwo: string;
  damagegadgets: string;
  kounarmed: number;
  kothrownitem: number;
  koweaponone: number;
  koweapontwo: number;
  kogadgets: number;
  timeheldweaponone: number;
  timeheldweapontwo: number;
  xp: number;
  level: number;
  xp_percentage: number;
}

export interface IPlayerStatsClan {
  clan_name: string;
  clan_id: number;
  clan_xp: string;
  personal_xp: number;
}
