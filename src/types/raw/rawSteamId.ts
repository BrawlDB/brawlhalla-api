export interface IRawSteamId {
  brawlhalla_id: number;
  name: string;
}
