import { TValidRegions, TValidTier } from '../index';

export interface IRawPlayerRankings {
  rank: string;
  name: string;
  brawlhalla_id: number;
  // The best legend ID, best = highest win rate
  best_legend: number;
  best_legend_games: number;
  best_legend_wins: number;
  rating: number;
  tier: TValidTier;
  games: number;
  wins: number;
  region: TValidRegions;
  peak_rating: number;
  twitch_name?: string | null;
}
