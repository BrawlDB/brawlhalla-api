import { TClanRank } from '../types';

export interface ClanMember {
  brawlhallaID: number;
  name: string;
  rank: TClanRank;
  contributedExperience: number;
  joinDate: number;
}
