import { ClanMember } from './ClanMember';

export interface Clan {
  name: string;
  level: number;
  xp: number;
  creationDate: number;
  members: ClanMember[];
}
