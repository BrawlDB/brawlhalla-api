import { LegendRankedStats as ILegendRankedStats, TValidDivision, TValidTier } from '../../types';

import { LegendUtils } from '../../utils/LegendUtils';
import { Legend } from '../legend';
import { RawPlayerRankedLegendStats } from '../../types/raw/rawPlayerRankedStats';

export class LegendRankedStats implements ILegendRankedStats {
  public legend: Legend;
  public rating: number;
  public peakRating: number;

  public tier: TValidTier;
  public division: TValidDivision;

  public games: number;
  public wins: number;

  public get defeats(): number {
    return this.games - this.wins;
  }

  public constructor(data: RawPlayerRankedLegendStats) {
    this.legend = LegendUtils.findLegendById(data.legend_id);
    this.rating = data.rating;
    this.peakRating = data.peak_rating;

    const tierAndDivision: Array<TValidTier | TValidDivision> = data.tier.split(' ') as Array<
      TValidTier | TValidDivision
    >;
    // Parsing the tier and division
    this.tier = tierAndDivision[0] as TValidTier;
    this.division = tierAndDivision.length > 0 ? (tierAndDivision[1] as TValidDivision) : 1;

    this.games = data.games;
    this.wins = data.wins;
  }
}
