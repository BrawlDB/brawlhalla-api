import { PlayerStats as IPlayerStats, Weapon as IWeapon } from '../../types';
import { weapons } from '../../data/weapons';
import { SimpleClan } from '../clan/simpleClan';
import { IPlayerStatsLegend, IRawPlayerStats } from '../../types/raw/rawPlayerStats';
import { LegendStats } from './legendStats';
import { WeaponStats } from './weaponStats';

export class PlayerStats implements IPlayerStats {
  public brawlhallaID: number;
  public name: string;
  public xp: number;
  public level: number;
  public xpPercentage: number;
  public games: number;
  public wins: number;

  // Damage
  public damageDealtByBomb: number;
  public damageDealtByMine: number;
  public damageDealtBySpikeball: number;
  public damageDealtBySidekick: number;

  public snowballHits: number;

  // Kos
  public koByBomb: number;
  public koByMine: number;
  public koBySpikeball: number;
  public koBySidekick: number;
  public koBySnowball: number;

  public legendStats: LegendStats[];
  public weaponStats: WeaponStats[];

  public clan?: SimpleClan;

  public constructor(data: IRawPlayerStats) {
    this.brawlhallaID = data.brawlhalla_id;
    this.name = data.name;
    this.xp = data.xp;
    this.level = data.level;
    this.xpPercentage = data.xp_percentage;
    this.games = data.games;
    this.wins = data.wins;

    // Damage
    this.damageDealtByBomb = +data.damagebomb;
    this.damageDealtByMine = +data.damagemine;
    this.damageDealtBySpikeball = +data.damagespikeball;
    this.damageDealtBySidekick = +data.damagesidekick;

    this.snowballHits = data.hitsnowball;

    // Kos
    this.koByBomb = data.kobomb;
    this.koByMine = data.komine;
    this.koBySpikeball = data.kospikeball;
    this.koBySidekick = data.kosidekick;
    this.koBySnowball = data.kosnowball;

    this.legendStats = data.legends.map(
      (rawLegend: IPlayerStatsLegend): LegendStats => new LegendStats(rawLegend)
    );

    this.weaponStats = weapons.map((weapon: IWeapon) => new WeaponStats(weapon, this.legendStats));

    if (data.clan !== undefined) {
      this.clan = new SimpleClan(data.clan);
    }
  }
}
