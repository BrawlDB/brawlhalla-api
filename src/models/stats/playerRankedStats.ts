import {
  PlayerRankedStats as IPlayerRankedStats,
  TValidDivision,
  TValidRegions,
  TValidTier
} from '../../types';
import {
  Raw2v2Team,
  RawPlayerRankedLegendStats,
  RawPlayerRankedStats
} from '../../types/raw/rawPlayerRankedStats';
import { LegendRankedStats } from './legendRankedStats';
import { PlayerTeamStats } from './playerTeamStats';

export class PlayerRankedStats implements IPlayerRankedStats {
  public brawlhallaID: number;
  public name: string;

  public region: TValidRegions;
  public globalRank: number;
  public regionRank: number;

  public rating: number;
  public peakRating: number;
  public tier: TValidTier;
  public division: TValidDivision;

  public legends: LegendRankedStats[];
  public teams: PlayerTeamStats[];

  public constructor(data: RawPlayerRankedStats) {
    this.brawlhallaID = data.brawlhalla_id;
    this.name = data.name;

    this.region = data.region;
    this.globalRank = data.global_rank;
    this.regionRank = data.region_rank;
    this.rating = data.rating;
    this.peakRating = data.peak_rating;

    const tierAndDivision: [TValidTier, TValidDivision] = data.tier.split(' ') as [
      TValidTier,
      TValidDivision
    ];
    // Parsing the tier and division
    this.tier = tierAndDivision[0];
    // Diamond does not have a division, so it is implicitly 1
    this.division = tierAndDivision.length > 0 ? tierAndDivision[1] : 1;

    this.legends = data.legends.map(
      (rawLegend: RawPlayerRankedLegendStats): LegendRankedStats =>
        new LegendRankedStats(rawLegend)
    );

    this.teams = data['2v2'].map((rawTeam: Raw2v2Team) => new PlayerTeamStats(rawTeam));
  }
}
