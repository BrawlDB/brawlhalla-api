import {
  LegendStats as ILegendStats,
  Weapon as IWeapon,
  WeaponStats as IWeaponStats,
} from "../../types";

export class WeaponStats implements IWeaponStats {
  public weapon: IWeapon;

  public games: number;
  public wins: number;
  public kos: number;
  public time: number;

  public constructor(weapon: IWeapon, legendsStats?: ILegendStats[]) {
    this.weapon = weapon;
    this.games = 0;
    this.wins = 0;
    this.kos = 0;
    this.time = 0;

    if (legendsStats !== undefined) {
      legendsStats.forEach((legendStats: ILegendStats) => {
        this.addLegendStats(legendStats);
      });
    }
  }

  public addLegendStats(legendStats: ILegendStats): void {
    const isFirstWeapon: boolean =
      legendStats.legend.firstWeapon === this.weapon;
    const isSecondWeapon: boolean =
      legendStats.legend.secondWeapon === this.weapon;
    const isUnarmed: boolean = this.weapon.name === "Unarmed";
    if (isFirstWeapon || isSecondWeapon || isUnarmed) {
      this.games += legendStats.games;
      this.wins += legendStats.wins;

      if (isFirstWeapon) {
        this.kos += legendStats.koByFirstWeapon;
        this.time += legendStats.timeHeldFirstWeapon;
      } else if (isSecondWeapon) {
        this.kos += legendStats.koByFirstWeapon;
        this.time += legendStats.timeHeldSecondWeapon;
      } else {
        this.kos += legendStats.koByUnarmed;
        this.time +=
          legendStats.matchTime -
          (legendStats.timeHeldFirstWeapon + legendStats.timeHeldSecondWeapon);
      }
    }
  }
}
