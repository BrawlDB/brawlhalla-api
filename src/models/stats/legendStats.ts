import { LegendStats as ILegendStats } from '../../types';

import { LegendUtils } from '../../utils/LegendUtils';
import { Legend } from '../legend';
import { IPlayerStatsLegend } from '../../types/raw/rawPlayerStats';

export class LegendStats implements ILegendStats {
  public legend: Legend;

  public xp: number;
  public level: number;
  public xpPercentage: number;

  public damageDealt: number;
  public damageTaken: number;
  public kos: number;
  public falls: number;
  public suicides: number;
  public teamKos: number;
  public matchTime: number;
  public games: number;
  public wins: number;

  public get defeats(): number {
    return this.games - this.wins;
  }

  // Damage
  public damageDealtByUnarmed: number;
  public damageDealtByThrownItems: number;
  public damageDealtByFirstWeapon: number;
  public damageDealtBySecondWeapon: number;
  public damageDealtByGadgets: number;

  // Kos
  public koByUnarmed: number;
  public koByThrownItems: number;
  public koByFirstWeapon: number;
  public koBySecondWeapon: number;
  public koByGadgets: number;

  public timeHeldFirstWeapon: number;
  public timeHeldSecondWeapon: number;

  public constructor(data: IPlayerStatsLegend) {
    this.legend = LegendUtils.findLegendById(data.legend_id);

    this.xp = data.xp;
    this.level = data.level;
    this.xpPercentage = data.xp_percentage;

    this.damageDealt = +data.damagedealt;
    this.damageTaken = +data.damagetaken;
    this.kos = data.kos;
    this.falls = data.falls;
    this.suicides = data.suicides;
    this.teamKos = data.teamkos;
    this.matchTime = data.matchtime;
    this.wins = data.wins;
    this.games = data.games;

    this.damageDealtByUnarmed = +data.damageunarmed;
    this.damageDealtByThrownItems = +data.damagethrownitem;
    this.damageDealtByFirstWeapon = +data.damageweaponone;
    this.damageDealtBySecondWeapon = +data.damageweapontwo;
    this.damageDealtByGadgets = +data.damagegadgets;

    this.koByUnarmed = +data.kounarmed;
    this.koByThrownItems = +data.kothrownitem;
    this.koByFirstWeapon = +data.koweaponone;
    this.koBySecondWeapon = +data.koweapontwo;
    this.koByGadgets = +data.kogadgets;

    this.timeHeldFirstWeapon = data.timeheldweaponone;
    this.timeHeldSecondWeapon = data.timeheldweapontwo;
  }
}
