import {
  PlayerTeamStats as IPlayerTeamStats,
  TValidDivision,
  TValidRegions,
  TValidTier
} from '../../types';
import { Raw2v2Team } from '../../types/raw/rawPlayerRankedStats';

export class PlayerTeamStats implements IPlayerTeamStats {
  // tslint:disable:no-magic-numbers
  private static numberToRegion: Map<number, TValidRegions> = new Map<number, TValidRegions>([
    [2, 'us-e'],
    [3, 'eu'],
    [4, 'sea'],
    [5, 'brz'],
    [6, 'aus'],
    [7, 'us-w']
  ]);
  // tslint:enable:no-magic-numbers

  public brawlhallaIdOne: number;
  public brawlhallaIdTwo: number;

  public teamName: string;
  public region: TValidRegions;

  public rating: number;
  public peakRating: number;
  public tier: TValidTier;
  public division: TValidDivision;
  public globalRank: number;

  public games: number;
  public wins: number;

  public brawlhallaNameOne: string;
  public brawlhallaNameTwo: string;

  public get defeats(): number {
    return this.games - this.wins;
  }

  public constructor(data: Raw2v2Team) {
    this.brawlhallaIdOne = data.brawlhalla_id_one;
    this.brawlhallaIdTwo = data.brawlhalla_id_two;
    this.teamName = data.teamname;
    const possibleNames: string[] = this.teamName.split('+');

    this.brawlhallaNameOne = possibleNames[0];
    this.brawlhallaNameTwo = possibleNames[1];
    // tslint:disable-next-line:no-magic-numbers
    if (possibleNames.length > 2) {
      console.warn(`Potential invalid team name detected: ${this.teamName}`);
    }

    this.region = PlayerTeamStats.numberToRegion.get(data.region) as TValidRegions;

    this.rating = data.rating;
    this.peakRating = data.peak_rating;
    this.globalRank = data.global_rank;

    const tierAndDivision: [TValidTier, TValidDivision] = data.tier.split(' ') as [
      TValidTier,
      TValidDivision
    ];
    // Parsing the tier and division
    this.tier = tierAndDivision[0];
    // Diamond does not have a division, so it is implicitly 1
    this.division = tierAndDivision.length > 0 ? tierAndDivision[1] : 1;

    this.games = data.games;
    this.wins = data.wins;
  }
}
