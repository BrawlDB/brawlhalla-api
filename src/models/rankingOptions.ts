import { RankingOptions as IRankingOptions, TValidBrackets, TValidRegions } from '../types';

export class RankingOptions implements IRankingOptions {
  public bracket: TValidBrackets;
  public region: TValidRegions;
  /**
   * The player name which is currently being searched
   * Note: The search is based on the first characters of the player name
   * Therefore, searching for a middle part of the name will not work.
   */
  public name?: string;

  /**
   * The page number
   */
  public page: number;

  public constructor(
    bracket?: TValidBrackets,
    region?: TValidRegions,
    page?: number | null,
    name?: string
  ) {
    this.bracket = bracket === undefined ? '1v1' : bracket;
    this.region = region === undefined ? 'all' : region;
    this.page = page === undefined || page === null ? 1 : page;
    this.name = name === undefined ? '' : name;
  }
}
