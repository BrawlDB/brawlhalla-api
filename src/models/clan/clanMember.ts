import { ClanMember as IClanMember, TClanRank } from '../../types';
import { RawClanMember } from '../../types/raw/rawClan';

export class ClanMember implements IClanMember {
  public brawlhallaID: number;
  public name: string;
  public rank: TClanRank;
  public contributedExperience: number;
  public joinDate: number;

  public constructor(data: RawClanMember) {
    this.brawlhallaID = data.brawlhalla_id;
    this.name = data.name;
    this.rank = data.rank;
    this.contributedExperience = data.xp;
    this.joinDate = data.join_date;
  }
}
