// Clan information contained when searching a user's stats
import { SimpleClan as ISimpleClan } from '../../types';
import { IPlayerStatsClan } from '../../types/raw/rawPlayerStats';

export class SimpleClan implements ISimpleClan {
  public name: string;
  public id: number;
  public xp: number;
  public personalXp: number;

  public constructor(data: IPlayerStatsClan) {
    this.name = data.clan_name;
    this.id = data.clan_id;
    this.xp = +data.clan_xp;
    this.personalXp = data.personal_xp;
  }
}
