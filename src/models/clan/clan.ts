import { Clan as IClan } from '../../types';

import { RawClan, RawClanMember } from '../../types/raw/rawClan';
import { ClanMember } from './clanMember';

export class Clan implements IClan {
  public name: string;
  public level: number;
  public xp: number;
  public creationDate: number;
  public members: ClanMember[];

  public constructor(data: RawClan) {
    this.name = data.clan_name;
    this.xp = +data.clan_xp;
    this.level = 0;
    this.creationDate = data.clan_create_date;
    this.members = data.clan.map(
      (rawClanMember: RawClanMember): ClanMember => new ClanMember(rawClanMember)
    );
  }
}
