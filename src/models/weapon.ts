import { TWeaponName } from '../types';

export class Weapon {
  public readonly id: number;
  public readonly name: TWeaponName;

  public constructor(id: number, name: TWeaponName) {
    this.id = id;
    this.name = name;
  }
}
