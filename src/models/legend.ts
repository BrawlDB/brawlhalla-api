import { TLegendName } from '../types';
import { Weapon } from './weapon';

// TODO: Add description, lore, stats, skins, etc.
export class Legend {
  public readonly id: number;
  public readonly name: TLegendName;
  public readonly firstWeapon: Weapon;
  public readonly secondWeapon: Weapon;

  /**
   * Creates a new legend
   * @param id The legend unique identifier
   * @param name The legend name
   * @param firstWeapon The legend's first weapon
   * @param secondWeapon The legend's second weapon
   */
  public constructor(id: number, name: TLegendName, firstWeapon: Weapon, secondWeapon: Weapon) {
    this.id = id;
    this.name = name;
    this.firstWeapon = firstWeapon;
    this.secondWeapon = secondWeapon;
  }
}
