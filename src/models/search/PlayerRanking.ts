import {
  PlayerRanking as IPlayerRanking,
  TValidDivision,
  TValidRegions,
  TValidTier
} from '../../types';

import { LegendUtils } from '../../utils/LegendUtils';
import { Legend } from '../legend';
import { IRawPlayerRankings } from '../../types/raw/rawRankings';

export class PlayerRanking implements IPlayerRanking {
  public brawlhallaID: number;
  public name: string;
  public rank: number;
  public bestLegend: Legend;
  public bestLegendGames: number;
  public bestLegendWins: number;
  public rating: number;
  public peakRating: number;
  public tier: TValidTier;
  public division: TValidDivision;
  public games: number;
  public wins: number;
  public region: TValidRegions;

  public constructor(data: IRawPlayerRankings) {
    this.brawlhallaID = data.brawlhalla_id;
    this.name = data.name;
    this.rank = +data.rank;
    this.bestLegend = LegendUtils.findLegendById(data.best_legend);
    this.bestLegendGames = data.best_legend_games;
    this.bestLegendWins = data.best_legend_wins;
    this.rating = data.rating;
    this.peakRating = data.peak_rating;
    const [tier, division] = data.tier.split(' ');
    this.tier = tier as TValidTier;
    this.division = (+division as TValidDivision) || 0;
    this.games = data.games;
    this.wins = data.wins;
    this.region = data.region;
  }
}
