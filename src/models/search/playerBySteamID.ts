import { PlayerBySteamID as IPlayerBySteamID } from '../../types';

import { IRawSteamId } from '../../types/raw/rawSteamId';

export class PlayerBySteamID implements IPlayerBySteamID {
  public brawlhallaID: number;
  public name: string;

  public constructor(data: IRawSteamId) {
    this.brawlhallaID = data.brawlhalla_id;
    this.name = data.name;
  }
}
