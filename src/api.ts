import {
  Clan as IClan,
  PlayerBySteamID as IPlayerBySteamID,
  PlayerRankedStats as IPlayerRankedStats,
  PlayerRanking as IPlayerRanking,
  PlayerStats as IPlayerStats,
  RankingOptions as IRankingOptions,
  TValidBrackets,
  TValidRegions
} from './types';

import { Clan } from './models/clan/clan';
import { Legend } from './models/legend';
import { RawClan } from './types/raw/rawClan';
import { RawPlayerRankedStats } from './types/raw/rawPlayerRankedStats';
import { IRawPlayerStats } from './types/raw/rawPlayerStats';
import { IRawPlayerRankings } from './types/raw/rawRankings';
import { IRawSteamId } from './types/raw/rawSteamId';
import { PlayerBySteamID } from './models/search/playerBySteamID';
import { PlayerRanking } from './models/search/PlayerRanking';
import { PlayerRankedStats } from './models/stats/playerRankedStats';
import { PlayerStats } from './models/stats/playerStats';
import { isNumeric } from './utils/BaseTypes';
import { BrawlhallaRequest, IRequestArgument } from './utils/BrawlhallaRequest';
import { LegendUtils } from './utils/LegendUtils';
import { SteamApi } from './utils/steamApi';

export class BrawlhallaApi {
  /**
   * The API url
   */
  private static readonly apiUrl: string = 'https://api.brawlhalla.com';
  private readonly bhRequest: BrawlhallaRequest;
  private steamApi?: SteamApi;

  /**
   * The api key
   * Used in every request on the Brawlhalla API
   */
  private _brawlhallaApiKey: string;

  public set brawlhallaApiKey(apiKey: string) {
    this._brawlhallaApiKey = apiKey;
  }

  public get brawlhallaApiKey(): string {
    return this._brawlhallaApiKey;
  }

  public constructor(brawlhallaApiKey: string, steamApiKey?: string) {
    this._brawlhallaApiKey = brawlhallaApiKey;
    if (steamApiKey !== undefined) {
      this.steamApi = new SteamApi(steamApiKey);
    }
    this.bhRequest = new BrawlhallaRequest(brawlhallaApiKey, BrawlhallaApi.apiUrl);
  }

  /**
   * Performs a request on the Brawlhalla API to find a given player's stats
   * GET https://api.brawlhalla.com/player/{brawlhalla_id}/stats
   * @param  brawlhallaID The Brawlhalla ID of the player which you want the stats
   * @returns Stats of the player matching the Brawlhalla ID, or an error if this player does not exist
   */
  public async getPlayerStats(brawlhallaID: number): Promise<IPlayerStats> {
    const pathPrefix: string = 'player';
    const pathSuffix: string = 'stats';

    const requestResult: IRawPlayerStats = await this.bhRequest.doRequest<
      IRawPlayerStats
    >([pathPrefix, brawlhallaID.toString(), pathSuffix]);
    if (requestResult.name === undefined) {
      throw new Error('Invalid player id given!');
    } else {
      return new PlayerStats(requestResult);
    }

  }

  /**
   * Performs a request on the Brawlhalla API to find a given player's ranked stats
   * GET https://api.brawlhalla.com/player/{brawlhalla_id}/ranked
   * @param  brawlhallaID The Brawlhalla ID of the player which you want the stats
   * @returns Ranked stats of the player matching the Brawlhalla ID, or undefined if this player didn't play ranked
   */
  public async getPlayerRankedStats(brawlhallaID: number): Promise<IPlayerRankedStats | undefined> {
    const pathPrefix: string = 'player';
    const pathSuffix: string = 'ranked';

    const requestResult: RawPlayerRankedStats = await this.bhRequest.doRequest<
      RawPlayerRankedStats
    >([pathPrefix, brawlhallaID.toString(), pathSuffix]);
    if (requestResult.brawlhalla_id !== undefined) {
      return new PlayerRankedStats(requestResult);
    } else {
      return undefined;
    }
  }

  /**
   * Performs a request on the Brawlhalla API to find a given clan's information
   * GET https://api.brawlhalla.com/clan/{clan_id}
   * @param clanID The ID of the clan which you want to search
   * @returns The clan information of the clan with the given ID, or an error if this clan does not exist
   */
  public async getClanInfo(clanID: number): Promise<IClan> {
    const pathPrefix: string = 'clan';

    const requestResult: RawClan = await this.bhRequest.doRequest<RawClan>([
      pathPrefix,
      clanID.toString()
    ]);

    return new Clan(requestResult);
  }

  /**
   * Performs a request on the Brawlhalla API to find a Brawlhalla ID from a steam ID
   * @param steamID The SteamID of the player which you want to search
   * @returns The player name and steam id matching the corresponding steam ID
   */
  public async getBrawlhallaIdFromSteamId(steamID: string): Promise<IPlayerBySteamID> {
    const pathPrefix: string = 'search';
    const data: IRequestArgument = { steamid: steamID };

    const requestResult: IRawSteamId = await this.bhRequest.doRequest<IRawSteamId>(
      [pathPrefix],
      data
    );

    return new PlayerBySteamID(requestResult);
  }

  public async getRankings(options: IRankingOptions): Promise<IPlayerRanking[]> {
    const pathPrefix: string = 'rankings';

    const requestOptions: IRequestArgument = {};
    if (options.name !== undefined) {
      requestOptions.name = options.name;
    }

    const bracket: TValidBrackets = options.bracket === undefined ? '1v1' : options.bracket;
    const region: TValidRegions = options.region === undefined ? 'all' : options.region;
    const page: string = options.page === undefined ? '1' : options.page.toString();

    const requestResult: IRawPlayerRankings[] = await this.bhRequest.doRequest<
      IRawPlayerRankings[]
    >([pathPrefix, bracket, region, page], requestOptions);

    return requestResult.map(
      (rawRanking: IRawPlayerRankings) => new PlayerRanking(rawRanking)
    );
  }

  // Todo: Get the API stuff into the legend model
  public getLegendInfo(legendIdOrName: number | string): Legend {
    if (isNumeric(legendIdOrName)) {
      return LegendUtils.findLegendById(legendIdOrName);
    } else {
      return LegendUtils.findLegendByApproximateName(legendIdOrName);
    }
  }

  // Utils via the steam API
  public async getBhidFromSteamProfile(profileUrl: string): Promise<number> {
    if (this.steamApi === undefined) {
      throw new Error('The steam API key was not given upon construction of the class!');
    }
    const steamId: string = await this.steamApi.getSteamId(profileUrl);
    if (steamId === '') {
      throw new Error('Invalid profile URL given');
    }

    const bhProfile: PlayerBySteamID = await this.getBrawlhallaIdFromSteamId(steamId);
    if (bhProfile === undefined) {
      throw new Error('This Steam profile does not have an associated Brawlhalla account!');
    }

    return bhProfile.brawlhallaID;
  }
}
