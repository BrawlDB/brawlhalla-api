import { Weapon } from "../models/weapon";

// Weapon ID's are quite explicit, allowing magic numbers
/* tslint:disable:no-magic-numbers */
export const unarmed: Weapon = new Weapon(0, "Unarmed");
export const hammer: Weapon = new Weapon(1, "Hammer");
export const axe: Weapon = new Weapon(2, "Axe");
export const sword: Weapon = new Weapon(3, "Sword");
export const blasters: Weapon = new Weapon(4, "Blasters");
export const lance: Weapon = new Weapon(5, "Rocket Lance");
export const spear: Weapon = new Weapon(6, "Spear");
export const katar: Weapon = new Weapon(7, "Katar");
export const bow: Weapon = new Weapon(8, "Bow");
export const gauntlets: Weapon = new Weapon(9, "Gauntlets");
export const scythe: Weapon = new Weapon(10, "Scythe");
export const cannon: Weapon = new Weapon(11, "Cannon");
export const orb: Weapon = new Weapon(12, "Orb");
export const greatsword: Weapon = new Weapon(13, "Greatsword");
/* tslint:enable:no-magic-numbers */

export const weapons: Weapon[] = [
  unarmed,
  hammer,
  axe,
  sword,
  blasters,
  lance,
  spear,
  katar,
  bow,
  gauntlets,
  scythe,
  cannon,
  orb,
  greatsword
];
